"""The aim of this module is to provide functionality for preprocessing.

TODO: mark activities

With text data, this cleaning process can go on forever. 
There's always an exception to every cleaning step. 
So, we're going to follow the MVP (minimum viable product) approach - start simple and iterate.

This module mainly uses functionality of regular expressions. See:
    - https://docs.python.org/3/howto/regex.html
    - https://docs.python.org/3/library/re.html

After preprocessing meeting protcol should have the format
'
\begin{document:DOCUMENT ID}\n
\begin{activity:ACTIVITY ID}\n
Person X:\n
Meine Damen und Herren...\n
\behaviour{Verhalten}\n
Danke!\n
\end{activity:ACTIVITY ID}\n
\begin{activity:ACTIVITY ID2}\n
...
\endpage{PAGE ID}\n
...
\end{activity:ACTIVITY IDX}\n
\end{document:DOCUMENT ID}\n
'
Line breaks should only be before and after behaviours, after structuring
elements, after descriptions who is speaking, after speeches.
"""

import re


def cut_before_protocol_starts(text: str) -> str:
    """Cut off document description if it exists.

    Args:
        text (str): protocol

    Returns:
        str: remaining protocol
    """
    description = re.search(r'Deutscher Bundestag\n', text)

    if description:
        text = text[description.start():]

    return text


def get_protocol_header_pattern(text: str, period: int) -> re.Pattern:
    """Generate the pattern for protocol header.

    Args:
        text (str): protocol
        period (int): Wahlperiode
        
    Returns:
        re.Pattern: regular expression pattern
    """
    text = cut_before_protocol_starts(text)

    text_information = text.split('\n')[:4]

    meeting = text_information[0]
    num_sitzung = (text_information[2]).split('.')[0]
    location_and_date = text_information[3]
    if location_and_date[-1] == ' ':
        location_and_date = location_and_date[:-1]

    return re.compile(rf'{meeting}.+?{period}.+?{num_sitzung}.+?{location_and_date.split(".")[0]}.+?{location_and_date.split(".")[1]}')


def remove_errornous_header_repetition(text: str, header: re.Pattern) -> str:
    """Remove an undesired header repetition.

    Args:
        text (str): protocol
        header (re.Pattern): header

    Returns:
        str: revised protocol
    """
    return re.sub(rf'({header.pattern}) +({header.pattern})', r'\1 ', text)


def add_whitespace_after_header(text: str, header: re.Pattern) -> str:
    """Add a whitespace after header pattern.

    Args:
        text (str): protocol.
        header (re.Pattern): header pattern

    Returns:
        str: revised protocol.
    """
    text = re.sub(rf'({header.pattern}) *', r'\1 ', text)
    text = re.sub(rf'({header.pattern}) +([0-9]+) +([0-9]+)\n', r'\1 \2\3\n', text)

    return text


def remove_page_quadrants(text: str) -> str:
    """Remage page quadrants (A) (B) (C) (D).

    Args:
        text (str): protocol.

    Returns:
        str: revised protocol.
    """
    text = re.sub(r' *\(A\) *\n*', '', text)
    text = re.sub(r' *\(B\) *\n*', '', text)
    text = re.sub(r' *\(C\) *\n*', '', text)
    text = re.sub(r' *\(D\) *\n*', '', text)

    return text


def cut_before_meeting_starts(text: str, text_id: int) -> str:
    """Cut off parts of a protocol of a meeting before it starts.

    Args:
        text (str): Protocol text.
        text_id (int): ID of document.

    Returns:
        str: Remaining document.
    """
    m = re.search(r'\\startpage{[0-9]*}', text)
    # if re.search(r'\nBeginn:.*Uhr.*\n', text) is None:
    #     print("Removed 'Uhr' from 'Beginn'")
    #     search = re.search(r'\nBeginn:.*\n', text)
    # else:
    #     search = re.search(r'\nBeginn:.*Uhr.*\n', text)

    return (
        f"\\begin{{document:{text_id}}}\n"
        + text[m.start():]
    )

def cut_after_meeting_ends(text: str, text_id: int) -> str:
    """Cut off parts of a protocol of a meeting after it ends.

    Args:
        text (str): Protocol text.
        text_id (int): ID of document.

    Returns:
        str: Remaining document.
    """

    search_a = re.search(r'\n\(Schluss(.+?)Uhr\).*\n', text)
    search_b = re.search(r'\n\(Schluss(.+?)\).*\n', text)
    search_c = re.search(r'\n\(Ende(.+?)Uhr\).*\n', text)
    search_d = re.search(r'\n\(Sitzungsende(.+?)Uhr\).*\n', text)

    if search_a is None:
        print("Removed 'Uhr' from 'Schluss'")
        search = search_b
    else:
        search = search_a

    if (search_a is None) and (search_b is None):
        print("Changed 'Schluss' to 'Ende'")
        search = search_c

    if (search_a is None) and (search_b is None) and (search_c is None):
        print("Changed 'Schluss' to 'Sitzungsende'")
        search = search_d

    return (
        text[: search.start()]
        + f"\n\\end{{document:{text_id}}}"
    )


def cut_meeting(text: str, text_id: int) -> str:
    """Cut off parts of a meeting that do not contain speeches.

    Args:
        text (str): Protocol text.
        text_id (int): ID of document.

    Returns:
        str: Remaining document.
    """
    return cut_after_meeting_ends(
        cut_before_meeting_starts(text, text_id), text_id)


def mark_pages(text: str, header: re.Pattern, protocol_num: int) -> str:
    """Replace header and page navigation by page numbering.
    
    Args:
        text (str): Protocol text.

    Returns:
        str: Revised document.
    """

    # Check if header is at the beginning or end of page
    pattern_decimal = re.compile(r'[1-9][0-9\-]*')
    pattern_roman = re.compile(r'[IVXL]+')

    first_page = re.search(fr'{header.pattern} (.*)\n{protocol_num}\. Sitzung\n', text).group(0).split('\n')[0].split(' ')[-1]
    if (not pattern_decimal.match(first_page)) and (not pattern_roman.match(first_page)):
        first_page = re.search(fr'(.*)\n{header.pattern} (.*)\n{protocol_num}\. Sitzung\n', text).group(0).split('\n')[0]

    if pattern_decimal.match(first_page):
        page_mark = 'startpage'
    elif pattern_roman.match(first_page):
        page_mark = 'endpage'
    else:
        print('Undefined header position.')

    # Page number after header
    text = re.sub(rf'({header.pattern}) *([1-9][0-9\-]*)\n', fr'\\{page_mark}{{\2}}\n', text)
    text = re.sub(rf'({header.pattern}) *([IVXL]+)\n', fr'\\{page_mark}{{\2}}\n', text)

    # Page number before header
    text = re.sub(rf'\n([1-9][0-9\-]*) *({header.pattern}) *\n', fr'\n\\{page_mark}{{\1}}\n', text)
    text = re.sub(rf'\n([IVXL]+) *({header.pattern}) *\n', fr'\n\\{page_mark}{{\1}}\n', text)

    # Page number in seperate line
    text = re.sub(rf'\n({header.pattern}) *\n([1-9][0-9\-]*)\n', fr'\n\\{page_mark}{{\2}}\n', text)
    text = re.sub(rf'\n([1-9][0-9\-]*) *\n({header.pattern}) *\n', fr'\n\\{page_mark}{{\1}}\n', text)

    # Remove remaining headers
    text = re.sub(rf'\n({header.pattern}) *\n', r'\n', text)

    # Unify page numbering
    text = re.sub(r'\\endpage(.*?)\n(.*?)\\endpage(.*?)\n', r'\\startpage\3\n\2\\endpage\3\n', text, flags=re.DOTALL)

    return text


def remove_repeated_whitespaces(text: str) -> str:
    """Remove repeated whitespaces in text.
    
    Args:
        text (str): Protocol text.

    Returns:
        str: Revised document.
    """
    text = re.sub(r" +", " ", text)
    text = re.sub(r"\n+", "\n", text)
    text = re.sub(r" \n", "\n", text)

    return text


def remove_whitespaces_before_punctation(text: str) -> str:
    """Remove all whitespaces before punctation.

    Args:
        text (str): Protocol text.

    Returns:
        str: Revised document.
    """
    text = re.sub(r" +([.!?,:;/-])", r'\1', text)

    return text

def remove_hyphenation(text: str) -> str:
    """Remove all hyphenations.

    Args:
        text (str): protocol text

    Returns:
        str: revised text
    """
    return re.sub(r'([a-zßéäöü0-9])- *\n([a-zßéäöü])', r'\1\2', text, flags=re.IGNORECASE)


def remove_line_breaks(text: str) -> str:
    """Remove unwanted line breaks.
    
    Args:
        text (str): Protocol text.

    Returns:
        str: Revised document.
    """    
    # Line break after letter or number
    # text = re.sub(r'([a-zßéäöü0-9]) *\n([^\(^\\].*?[^:]\n)', r'\1 \2', text, flags=re.IGNORECASE)
    # text = re.sub(r'([a-zßéäöü0-9]) *\n([a-zßéäöü0-9A-ZÄÖÜ].*?[^:^\n]\n)', r'\1 \2', text, flags=re.IGNORECASE)

    # Line break after finished sub-clause.
    # text = re.sub(r'([,;…–]) *\n([^\(^\\])', r'\1 \2', text)

    # Line break after finished clause.
    # text = re.sub(r'([.!?“,;…–]) *\n([^\(^\\].*?[^:]\n)', r'\1 \2', text)

    # Line break after whitespace
    # text = re.sub(r' \n([^\(^\\])', r' \1', text)
    # Line break after text navigation
    # text = re.sub(r'([A-ZÄÖÜ]) *\n([^\(^\\])', r'\1 \2', text)
    # text = re.sub(r'([0-9]) *\n(\([A-D]\))', r'\1 \2', text)
    # text = re.sub(r'( *\n*)(\(.\))( *\n*)', r' \2 ', text)
    # Line break after slash and brackets
    # text = re.sub(r'([\]/])\n([^\(^\\])', r'\1\2', text)  # Also \} ?

    # Line break after not speaker related ":", Suppose that each spaeker has a title in ().
    # text = re.sub(r'([^\)]):\n([A-Z0-9ÄÖÜ„…–])', r'\1: \2', text)
    
    # Zahl gefolgt von Text in Klammern
    # text = re.sub(r' *\n\(([0-9])([^\^\\)]+)\)\n', r' (\1\2) ', text)
    # Line breaks followed by brackets without line break after closing bracket
    # text = re.sub(r' *\n\(([^\)]+)\)([^\n])', r' (\1)\2', text)

    return text


def mark_behaviour(text: str) -> str:
    """Replace behaviour strings with structural element.

    Args:
        text (str): Protocol text.

    Returns:
        str: Revised document.
    """
    return re.sub(r'\n\(([^\(]+?)\)\n', r'\n\\behaviour{\1}\n', text)


def remove_line_breaks_in_behaviours(text:str) -> str:
    text = re.sub(r'\\behaviour{([^}]*?)/\n([^}]*?)}', r'\\behaviour{\1/\2}', text)
    text = re.sub(r'\\behaviour{([^}]*?): *\n([^}]*?)}', r'\\behaviour{\1: \2}', text)
    text = re.sub(r'\\behaviour{([^}]*?)\n([^}]*?)}', r'\\behaviour{\1 \2}', text)

    return text


def mark_speaker(text, activities):

    re.sub(r'\n(.*\)):\n', r'\n\1:\n', text)

    return None


def preprocess_protocol(text: str, text_id: int, wahlperiode: int, protocol_num: int) -> str:
    """Preprocess protocol text with dedicated pipeline.
    
    Before line breaks are removed, the protocol is structured:
        (TODO 1. Remove current speaker which is placed next to page breaks)
        2. Remove page navigation quadrants
        3. Remove repeated whitespaces
        (TODO 4. Remove foot notes)
        5. Mark page breaks and remove header strings
        6. Extract in person meeting
        7. Mark behaviour
        9. Mark speaker
        10. Clean up text

    Args:
        text (str): Protocol text.
        text_id (int): ID of protocol.
    
    Returns:
        str: Revised document.
    """

    text = remove_page_quadrants(text)

    text = remove_repeated_whitespaces(text)
    text = remove_whitespaces_before_punctation(text)

    header = get_protocol_header_pattern(text, wahlperiode)
    text = remove_errornous_header_repetition(text, header)
    text = add_whitespace_after_header(text, header)
    text = mark_pages(text, header, protocol_num)

    text = cut_meeting(text, text_id)

    text = mark_behaviour(text)
    text = remove_hyphenation(text)
    text = remove_line_breaks_in_behaviours(remove_line_breaks_in_behaviours(remove_line_breaks_in_behaviours(text)))

    # text = mark_speaker(text)

    # text = remove_repeated_whitespaces(text)
    # text = remove_line_breaks(text)
    # text = remove_repeated_whitespaces(text)

    # text = remove_whitespaces_before_punctation(text)
    
    return text
