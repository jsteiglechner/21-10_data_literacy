"""The aim of this module is to provide functionality for data I/O."""

import json


def load_json(file_path: str) -> dict:
    """Load json from file path in dat.

    Args:
        file_path (str): Relative path to file.

    Returns:
        dict: Content of file.
    """
    dat_src = './dat/' + file_path
    with open(dat_src) as json_file:
        json_data = json.load(json_file)
    return json_data


def save_json(file_path: str, content: dict) -> None:
    """Save content in json.

    Args:
        file_path (str): Path to target destination.
        content (dict): Content to write in file.
    """
    with open('./dat/' + file_path, 'w') as json_file:
        json.dump(content, json_file, indent=2)