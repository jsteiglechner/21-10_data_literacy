# import 
# Packages
import os
import re

import pandas as pd
import numpy as np


# functions
def find_protocol_id(text: str) -> int:
    """Find protocol ID of Document

    Args:
        text (str): protocol-text

    Returns:
        int: protocol ID
    """
    id = re.findall(r'\\begin{document:([0-9]*)}\n', text)
    return int(id[0])

def find_party_person(text: str, PARTIES: list) -> list:
    """Find behaviour of single person

    Args:
        text (str): behaviour string

    Returns:
        list: ['person','content']
    """
    person = []
    for party in PARTIES:
        # Find all single persons (can have: Abg. / Dr. / or city + Name & Party)
        party_search_abg_dr_name_city_party = re.search(rf'(Abg\S*|) *(Dr\S*|) *(\w*|^\w*) *\w* \w* (\[\w*\]|) *\[{party}\].*$',text)
        if party_search_abg_dr_name_city_party:
            string = party_search_abg_dr_name_city_party.group(0)
            if (": " in string): # if there is additional content, append it
                split = re.split(r': ',string)
                person.append(party)
                person.append(split[0])
                person.append(split[1])
            elif (":-" in string): # if there is additional content, append it
                split = re.split(r':-',string)
                person.append(party)
                person.append(split[0])
                person.append(split[1])
            else:
                person.append(party)
                person.append(string)
                person.append("None")
    return person

def find_party_abgeordneten(text: str, PARTIES: list) -> list:
    """Find all parties with abgeordneten Beteiligung

    Args:
        text (str): behaviour string
        PARTIES (list): PARTIES

    Returns:
        list: return all parties with Abgeordneten Beteiligung
    """
    parties = []
    for party in PARTIES:
        party_search = re.search(rf'Abg\S* \w* *({party})',text)
        if party_search:
            parties.append(party)
    return parties

def find_party_all(text: str, PARTIES: list) -> list:
    """Find party behaviour where all participated

    Args:
        text (str): preprocessed protocol-text
        PARTIES (list): Parties

    Returns:
        list: parties assigned to each behaviour
    """
    parties = []
    for party in PARTIES:
        party_search = re.search(rf'[^\[]{party}([^\]]|$)',text)
        if party_search:
            parties.append(party)
    return parties

def split_behaviour(text: str) -> list:
    """split behaviour string if "-" is present

    Args:
        text (str): entire behaviour string

    Returns:
        list: split string if multiple behaviours
    """
    split = re.split(r'(–)',text)
    if len(split) > 1:
        split.remove('–')
    return split

def find_behaviour(text: str, behaviours: list) -> list:
    """find given behaviours in behaviour string

    Args:
        text (str): behaviour string
        behaviours (list): behaviours

    Returns:
        list: behaviour
    """
    list_behaviours = []
    for behaviour in behaviours:
        behaviour_search = re.search(rf'{behaviour}\S*',text)
        if behaviour_search:
            list_behaviours.append(behaviour_search.group(0))
    if len(list_behaviours) > 1:
        behaviour_string = ' & '.join(list_behaviours)
    elif len(list_behaviours) == 1:
        behaviour_string = list_behaviours[0]
    else: 
        behaviour_string = "None"
    return behaviour_string

def create_df_behaviour(text: str,PARTIES: list,BEHAVIOURS: list) -> pd.DataFrame:
    """analyze text on behaviour and assign to dataframe

    Args:
        text (str): preprocessed protocol-text
        PARTIES (list): all parties to analyze
        BEHAVIOURS (list): all behaviours to analyze

    Returns:
        df (pd.DataFrame): df_behaviour
    """
    # create df_behaviour
    columns = ["Behaviour ID","Protocol ID", "Activity ID","Speaker","Party","Actor","Behaviour","Content"]
    df_behaviour = pd.DataFrame(columns=columns)
    protocol_id = find_protocol_id(text)
    behaviours = re.findall(r'\n\\behaviour{([^\(]+?)}\n', text)
    ind = 0
    for m in behaviours:
        split = split_behaviour(m)
        # for multiple splits in one behaviour string
        for subsplit in split:
            parties_all = find_party_all(subsplit,PARTIES)
            parties_abgeordneten = find_party_abgeordneten(subsplit,PARTIES)
            parties_all_only = list(set(parties_all) - set(parties_abgeordneten))
            parties_person = find_party_person(subsplit,PARTIES)
            behaviour = find_behaviour(subsplit,BEHAVIOURS)
          
        # Create behaviour for each party in one behaviour stirng
            if parties_all_only:
                for party in parties_all_only:
                    party_data = [ind, protocol_id, np.nan, np.nan, party, 'all', behaviour, "None"]
                    df_temp = pd.DataFrame(data=[party_data],columns=columns)
                    df_behaviour = df_behaviour.append(df_temp)
                    # print(party_data)
            if parties_abgeordneten:
                for party in parties_abgeordneten:
                    party_data = [ind, protocol_id, np.nan, np.nan, party, 'abgeordneten', behaviour, "None"]
                    df_temp = pd.DataFrame(data=[party_data],columns=columns)
                    df_behaviour = df_behaviour.append(df_temp)
                    # print(party_data)
            if parties_person:
                party_data = [ind, protocol_id, np.nan, np.nan, parties_person[0] , parties_person[1], behaviour, parties_person[2]]
                df_temp = pd.DataFrame(data=[party_data],columns=columns)
                df_behaviour = df_behaviour.append(df_temp)
                # print(party_data)
            ind += 1
    return df_behaviour

def get_lists_behaviour(df: pd.DataFrame, party: str, behaviours: list) -> list:
    """Get number of behaviour 
   
    Args:
        df (pd.DataFrame): df behaviour
        behaviour (list): Behaviours to look for

    Returns:
        list: list of weighted/absolute sum behaviour
    """ 
    behaviour_list_weighted = []
    behaviour_list_absolut = []
    total_list = []
    for behaviour in behaviours:
        party_not_all_behaviour = df.loc[(df["Party"] == party) & (df["Behaviour"] == behaviour) & (df["Actor"] != 'all')]["Behaviour ID"].to_list()
        party_all_behaviour = df.loc[(df["Party"] == party) & (df["Behaviour"] == behaviour) & (df["Actor"] == 'all')]["Behaviour ID"].to_list()
        # weigh list entry
        len_not_all = len(party_not_all_behaviour)
        len_all = len(party_all_behaviour)
        # Append to lists
        behaviour_list_absolut.append(len_all+len_not_all)
        behaviour_list_weighted.append(int(len_all+len_not_all*0.5)) # Factor 0.5 for weighted
    total_list.append(behaviour_list_absolut)
    total_list.append(behaviour_list_weighted)
    return total_list
    

def create_df_behaviour_one_protocol(df: pd.DataFrame,PARTIES: list) -> pd.DataFrame:
    """Create dataframe for each dataframe protocol with number of pos/neg nehaviour 

    Args:
        df (pd.DataFrame): df behaviour
        PARTIES (list): all parties

    Returns:
        pd.DataFrame: df timeline behaviour
    """      
    columns = ["Protocol ID","Party","Behaviour","Behaviour Weighted"]
    behaviour_list = ["Beifall","Zustimmung","Heiterkeit","Zuruf","Lachen","Widerspruch","None"]
    create_df_behaviour_one_protocol = pd.DataFrame(columns=columns)
    for party in PARTIES:
        party_behaviour_list = get_lists_behaviour(df,party,behaviour_list)
        # index 0: behaviours not weighted, index 1: behaviours weighted
        data = [str(df["Protocol ID"].unique()), party, party_behaviour_list[0],party_behaviour_list[1]]
        df_behaviour_party = pd.DataFrame(data=[data],columns=columns)
        create_df_behaviour_one_protocol = create_df_behaviour_one_protocol.append(df_behaviour_party)
    return create_df_behaviour_one_protocol

def sum_behaviour_protocol(df: pd.DataFrame, PARTIES: list) -> list:
    """Sum up positive and negative behaviour for each party and return list (pos/neg)

    Args:
        df (pd.DataFrame): dataframe with positive and negative weightings
        PARTIES (list): Parties

    Returns:
        list: positive and negative list
    """
    pos_list = []
    neg_list = []
    for party in PARTIES:
        # add up weighted positive behaviour (all: 1, part/single: 0.5)
        # list should contain same order as parties / columns in later dataframe
        pos_list.append(len(df[df["Party"] == party]["Positive Weighted"][0][0]) + int(sum(df[df["Party"] == party]["Positive Weighted"][0][1])))
        neg_list.append(len(df[df["Party"] == party]["Negative Weighted"][0][0]) + int(sum(df[df["Party"] == party]["Negative Weighted"][0][1])))
    return pos_list, neg_list
