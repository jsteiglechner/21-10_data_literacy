"""The aim of this module is to extract relevant features of text.

Possible functions needed:
    - Get agenda items ('Tagesordnungspunkt')
"""

# def get_parties_behaviour_items(text: str) -> List[str]:
#     # OUTDATED!
#     indices = re.finditer(pattern=r'\n\([^\(]+\)\n', string=text)
#     behaviours = [text[i.start(): i.end()] for i in indices]
# 
#     # Filter behaviours if parties are part.
#     behaviours = list(filter(lambda b: any([party in b for party in PARTIES]), behaviours))
# 
#     return behaviours

