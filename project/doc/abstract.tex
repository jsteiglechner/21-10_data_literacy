\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2021

% ready for submission
\usepackage[preprint]{NeurIPS_style/neurips_2021}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{neurips_2021}

% to compile a camera-ready version, add the [final] option, e.g.:
%     \usepackage[final]{neurips_2021}

% to avoid loading the natbib package, add option nonatbib:
%    \usepackage[nonatbib]{neurips_2021}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage[colorlinks=true]{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
%\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{xcolor}         % colors
\usepackage{graphicx}

\title{Party Behaviour In The $19^{th}$ Legislative Period\\Of The Bundestag}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{%
  Florian Birk\\
  Matrikelnummer 3904763\\
  \texttt{florian.birk@student.uni-tuebingen.de} \\
  \And
  Julius Steiglechner\\
  ID 06\\
  \texttt{julius.steiglechner@uni-tuebingen.de} \\
}

\begin{document}

\maketitle

\begin{abstract}
	In this project, we propose to analyze all 239 \href{https://dip.bundestag.de/suche}{stenographic minutes of the German Bundestag} by general party behavior and intrinsic activities levels within each party over the time course of the $19^{th}$ legislative period (2017 - 2021). Based on meta and stenographic data, behaviours of single members, parts or the entire party were evaluated for each plenary session. Results reveal potential strategies in behavioural participation during this legislative period and different activity distributions of single members within each party. Further data analysis is part of future work to allow for more in depth investigation of parties relations, sympathy or interaction with regard to specific subject areas. The projects repository is openly available on \href{https://gitlab.tuebingen.mpg.de/jsteiglechner/21-10_data_literacy/-/tree/master/project}{GitLab}.
\end{abstract}

\section{Introduction}
Stenographic minutes provide a detailed documentation of all actions performed by each member and party in each plenary session (\textit{"Bundestagssitzung"}) of the German Bundestag. Each session follows a predefined agenda where members of each party can make contributions to the present items. Any issue-specific speech by members may be interrupted by intervening activities of other parties. Insight about party relations, conformity or disagreement with respect to agenda topics is given. This projects, aims to find primary patterns in behaviour like applause, consent, serenity, acclamation, laughter, dissent or interjection. Moreover, documented behaviour allows to separate contributions to whether they come from the entire party, parts or only a single member of the party. This allowed, additionally to investigating general party behaviour, to also investigate the distribution of single party member activity for each party.


\section{Methods}
\subsection{Data collection}
Data was requested from the Documentation and Information System for Parliamentary Materials (DPI), using \textit{http requests}. Data was received in form of \textit{.json} files and contained meta data as well as raw text files from each session. The information available from all 239 plenary session of the $19^{th}$ were collected and analyzed.

\subsection{Data analysis}
A main part of this work was data cleaning and feature assignment. Pythons \href{https://docs.python.org/3/library/re.html}{regular experssion toolbox} was primarily used to match patterns and modify the existing text. Firstly, parts of interest, excluding table of contents and attachments, were extracted followed by removal of unwanted whitespaces or linebreaks. After the first initial cleaning, behaviours, speakers and pagenumbers were marked within the text. In the final state, the text was class-divided by beginning of a document, starting of a new page, and identified behaviours. E.g., all behaviours could be clearly identified by matching content in round brackets and a new separating paragraph. One bracket could include multiple behaviours. In this case, each behaviour is separated by a hyphen and was automatically separated into two independent behaviours.\\

All further analysis was performed for the six main parties of the German Bundestag:  \textit{CDU/CSU, SPD, DIE LINKE, BÜNDNIS 90/DIE GRÜNEN, AfD, FDP}. Single member activities were identified by name and party affiliation (marked in square brackets within the behaviour string). The five most active members from each party were counted on their number of individual interactions over the time course of the entire legislative period (see. \autoref{fig_activity_members}).\\

Preprocessed protocols were further analyzed on general party behaviour, counting the number of each behaviour for the respective parties. Thereby, additional weighting was applied, where contributions of the entire party (\textit{'all'}) was weighted by a factor of 1, while contributions of parts or single members was weighted by a factor of 0.5. The behaviours analzed were \textit{applause, consent, serenity, acclamation, laughter, dissent or interjection} (German: Beifall, Zustimmung, Heiterkeit , Zuruf, Lachen, Widerspruch, Zwischenruf). In addition, the summed party behaviour for each plenary session was visualized (see \autoref{fig_general_party_behaviour}).

\section{Results}\label{sec:res}
Results in \autoref{fig_activity_members} shows the absolute activity of single members during the $19^{th}$ legislative period for each party. The most active members are shown on the bottom of each bar (red), with each stacked color, showing the absolute number of occurrence until the fifth most active member (black). The yellow bars show the accumulated occurence of single party members, not in the top 5. It is shown, that most active CDU/CSU members have a reduced number of occurrence compared to the absolute number of single member activities. This reflects an internal party behaviour with interactions from a broader range of members. DIE LINKE, has the party member with the highest absolute behaviour count during the entire legislative period. Furthermore, the AfD shows the highest cumulative occurrence of the top 5 most active party members. Each of these five members accounts for a relatively high number of interactions, even at position five of active members, compared to the other parties.\\

\autoref{fig_general_party_behaviour} shows the weighted (entire party: 1, parts/single member: 0.5) overall occurrence of different behaviours during the time course of the $19^{th}$ legislative period. The upper most subplot in \autoref{fig_general_party_behaviour} shows the total count of all behaviours for each plenary session of this legislative period. Overall, the total counts of behaviours follows a "zigzag" like pattern, reflecting sessions of potentially more controversial topics or a longer agenda. The second subplot represents the behaviour categorie \textit{applause ("Beifall")}, which is the most common behaviour in the Bundestag to support own parties speeches or other parties contributions. Figures for
serenity (Heiterkeit), dissent (Widerspruch) or interjection (Zwischenruf) show sporadic peaks for certain parties but no clear pattern along the entire legislative period. However, figures for acclamation (Zuruf) and laughter (Lachen), represent a clearly increased use by the AfD (purple) for these behavioural methods, compared to other parties. Acclamations seem to be a consistenly used tool by the AfD, as it is practiced over the entire legislative period.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{fig_activity_members.pdf}
	\caption{Absolute activity of single party members over the time course of the 19th legislative period. Red, blue, green, cyan and black represent the five most active in descending order. The yellow bar parts reflect the accumulated rest of single party members, not included in the top five.}	
	\label{fig_activity_members}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{fig_party_general_behaviour.pdf}
	\caption{Party behaviours over the time course of the 19th legislative period. The x-axis describes the pleanry session number. Please note the different scaling for each subplots due to the different frequency of each behaviour. The upper most subplot represents the accumulated behaviour of each party for the respective plenary session. CDU/CSU is shown in blue, SPD in orange, DIE LINKE in green, BÜNDNIS 90/DIE GRÜNEN in red, AfD in purple, FDP in brown and fraktionslos (not further evaluated in this work) in pink.}
	\label{fig_general_party_behaviour}
\end{figure}

\newpage
\section{Discussion}
Results of this project showed interesting behaviour patterns of single parties within the $19^{th}$ legislative period of the Bundestag. Limitations with regards to this project is the high susceptibility to text patterns. The number of exceptions that have to be considered is tremendous and require a lot of manual explorative testing of preprocessing scripts for the underlying documents. Therefore, we had to follow the minimum viable product (MVP) approach, starting simple and iterate. Time constraints did not allow for a more in-depth analysis of party relations, linguistic patterns or inter-party sympathy. This could be explored in future work. Nevertheless, behavioural analysis resulted already in meaningful results that reflect assumed party behaviour. There are clearly visible differences as repeated and continous interruption of other party speeches appear to be a potentially pre-defined strategy. How this effected future results and success in party selctions could be part of future work but is beyond the scope of this work.

\end{document}
