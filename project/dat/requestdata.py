"""
Python script loading "plenarprotokoll","plenarprotokoll-text", "aktivität" and "vorgang" from the 19th legislative period via http request. 
Source: Dokumentations- und Informationssystem für Parlamentsmaterialien (DIP) API

Data from all Bundestag sessions in the time frame: 24.10.17 - 07.09.21

Steiglechner & Birk, January 2022
"""

# Packages
import time
import requests #https://2.python-requests.org/en/master/
import json
import urllib.parse as parse
import os

file_path = os.path.dirname(os.path.abspath(__file__))

# Functions
def loaddata(num_load, base: str,description: str,add: str):
    source = base + '/' + description + add 
    r = requests.get(source).json()
    cursor = r['cursor']
    cursor_new = ''
    for i in range(num_load): 
        if not (cursor==cursor_new):
            cursor_new = cursor
            source_r_temp = source + '&cursor=' + parse.quote_plus(cursor_new)
            r_temp = requests.get(source_r_temp).json()
            r['documents'].extend(r_temp['documents'])
            cursor = r_temp['cursor']
        else:
            break
        if (i+1)%10 == 0:
            time.sleep(5) # Sleep for 5 seconds every 50 iteration, otherwise DIP breaks down
            print("Waiting for 5 seconds")
    print("Num reloads:", i+1)
    print(f"Number of Documents [{description}] loaded: {len(r['documents'])}")
    return r

def save_json(path: str,dict: dict,name: str):
    save_str = path + '\\' + name + '.json'
    with open(save_str,'w') as fp:
        json.dump(dict,fp)
    print(f"Save {save_str} ...")

# Data request
# Date: 07.09.21 - 24.10.17
# Documents are listed by date and ID (most recent on top)
# Only 100 entries can be requested at once, thus use cursor

base = 'https://search.dip.bundestag.de/api/v1/'
add_l19 = '?f.zuordnung=BT&f.datum.start=2017-10-24&f.datum.end=2021-09-07&apikey=N64VhW8.yChkBUIJeosGojQ7CSR2xwLf3Qy7Apw464'

# Plenarprotokoll-Meta (PM)
pm = loaddata(5,base,'plenarprotokoll',add_l19)
save_json(file_path,pm,'pm')
# Plenarprotokoll Text (PT)
pt = loaddata(40,base,'plenarprotokoll-text',add_l19)
save_json(file_path,pt,'pt')
# Aktivität-Meta (~ 402.640 aktivitaet)
a_reload = 402640 // 100 +1
a = loaddata(a_reload,base,'aktivitaet',add_l19)
save_json(file_path,a,'a')
# Vorgang-Meta (~ 51.790 vorgang)
v_reload = 51790 // 100 + 1
v = loaddata(v_reload,base,'vorgang',add_l19)
save_json(file_path,v,'v')


