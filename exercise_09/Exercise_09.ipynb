{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data Literacy\n",
    "#### University of Tübingen, Winter Term 2021/22\n",
    "## Exercise Sheet 9\n",
    "&copy; 2021 Prof. Dr. Philipp Hennig & Lukas Tatzel\n",
    "\n",
    "This sheet is **due on Monday 10 January 2022 at 10 am sharp (i.e. before the start of the lecture).**\n",
    "\n",
    "---\n",
    "\n",
    "## Fair PCA\n",
    "\n",
    "**What is this week's tutorial about?** \n",
    "As you have seen in the lecture, the application of the \"classical\" PCA can lead to unbalanced reconstruction errors for different groups of the dataset. For example, applying PCA to the [LFW dataset](http://vis-www.cs.umass.edu/lfw/), we find that the reconstruction error for women is larger than for men.\n",
    "\n",
    "PCA is a fundamental dimensionality reduction technique often used as a first step in the data-analysis pipeline. When such tools are used to make *actual* (e.g. political) decisions, it must be ensured that they are \"fair\". In the lecture, you discussed an approach that manipulates the data to match the two reconstruction errors. \n",
    "In this week's exercise, you will learn about and implement a slightly simplified version of Fair PCA ([NeurIPS 2018 publication](https://papers.nips.cc/paper/2018/hash/cc4af25fa9d2d5c953496579b75f6f6c-Abstract.html) by Samadi et al., [Fair PCA homepage](https://sites.google.com/site/ssamadi/home/fair-pca-homepage)) as a cleaner, more generally applicable alternative to \"messing\" with the dataset. Finally, you will check, if this fixes the issue by comparing PCA to Fair PCA on the LFW dataset. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preprocessing\n",
    "\n",
    "As a first step, we load the LFW dataset and normalize it to the interval $[0, 1]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load images\n",
    "m    = 13232     # Number of images\n",
    "dim  = (49, 36)  # Dimensions for each image\n",
    "data = np.zeros((m, dim[0]*dim[1]))\n",
    "for i in range(m):\n",
    "    data[i] = np.loadtxt(f'data/images/img{i}.txt').ravel()\n",
    "\n",
    "# Normalize to interval [0, 1]\n",
    "data = data / 255"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`M` contains the centered data. We split the data into male- and female-labeled faces and center these two groups *individually* which yields `A` and `B`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Centered data M\n",
    "M = data - np.mean(data, axis=0)\n",
    "\n",
    "# Split data into A (female) and B (male), center seperately\n",
    "sex = np.loadtxt('data/images/sex.txt')\n",
    "\n",
    "data_female = data[(sex == 0), :]\n",
    "A = data_female - np.mean(data_female, axis=0)\n",
    "\n",
    "data_male   = data[(sex == 1), :]\n",
    "B = data_male - np.mean(data_male, axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### \"Classical\" PCA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need PCA primarily as a reference to the Fair PCA algorithm. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pca(M, d):\n",
    "    \"\"\"Principal component analysis for centered data M using a d-dimensional PCA-space\"\"\"\n",
    "    \n",
    "    # Compute covariance matrix (assuming M is already centered)\n",
    "    C = np.cov(M, rowvar=False)\n",
    "    \n",
    "    # Apply SVD\n",
    "    Q, _, _ = np.linalg.svd(C, hermitian=True)\n",
    "    \n",
    "    # Return projection matrix\n",
    "    return Q[:, 0:d] @ Q[:, 0:d].T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A reconstruction can then be computed and visualized like this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choose parameters\n",
    "d = 100        # Dimensionality of PCA space\n",
    "img_index = 0  # Row-index of original image in data\n",
    "\n",
    "# Compute reconstruction\n",
    "P = pca(M, d)\n",
    "img_orig = data[img_index, :]\n",
    "img_pca = M[img_index, :] @ P + np.mean(data, axis=0)\n",
    "\n",
    "# Visualization\n",
    "def plot_img(ax, img):\n",
    "    \"\"\"Plot a single image into ax\"\"\"\n",
    "    ax.imshow(img.reshape(dim), cmap='gray', vmin=0, vmax=1)\n",
    "    ax.get_xaxis().set_visible(False)\n",
    "    ax.get_yaxis().set_visible(False)\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(5, 3))\n",
    "\n",
    "plot_img(ax1, img_orig)\n",
    "ax1.set_title(\"Original\")\n",
    "\n",
    "plot_img(ax2, img_pca)\n",
    "ax2.set_title(f\"Reconstruction (d = {d})\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fair PCA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Reconstruction Error:** \n",
    "First of all, we have to clearly define what we mean by a \"fair\" PCA algorithm - we need a mathematical criterion for \"fairness\". For this, we introduce the definition of the reconstruction error, which is a measure of the difference between the original images and their reconstruction. For two matrices $X$ and $Y$ of the same size, it is defined by\n",
    "$$\n",
    "\\text{error}(X, Y) := \\lVert X - Y \\rVert_\\text{F}^2 = \\sum_i \\sum_j \\lvert X_{i,j} - Y_{i,j} \\rvert^2.\n",
    "$$\n",
    "Note, that for the reconstruction error, it doesn't matter if $X$ and $Y$ are shifted by some average $T$, because the shifts cancel, i.e.\n",
    "$\n",
    "\\lVert (X - T) - (Y - T) \\rVert_\\text{F}^2\n",
    "= \\lVert X - Y \\rVert_\\text{F}^2\n",
    "$.\n",
    "\n",
    "**Loss:** \n",
    "Since we want our algorithm to perform equally for women and men, let's consider two subgroups $A \\in \\mathbb{R}^{m_1 \\times n}$ and $B \\in \\mathbb{R}^{m_2 \\times n}$. Applying PCA separately to $A$ and $B$ yields the reconstructions $\\hat{A}$ and $\\hat{B}$. In terms of the reconstruction error, these two reconstructions are optimal. So, when using a *different* reconstruction $U \\in \\mathbb{R}^{(m_1 + m_2) \\times n}$ ($U_A \\in \\mathbb{R}^{m_1 \\times n}$ contains rows of $U$ that correspond to group $A$; $U_B \\in \\mathbb{R}^{m_2 \\times n}$ contains rows of $U$ that correspond to group $B$), the reconstruction error on each group will increase. This is measured by the so-called reconstruction *loss*\n",
    "$$\n",
    "\\begin{align*}\n",
    "\\text{loss}(A, U_A) &= \\text{error}(A, U_A) - \\text{error}(A, \\hat{A}) \\geq 0 \\\\\n",
    "\\text{loss}(B, U_B) &= \\text{error}(B, U_B) - \\text{error}(B, \\hat{B}) \\geq 0.\n",
    "\\end{align*}\n",
    "$$\n",
    "For some reconstruction $U$, the loss measures by how much the reconstruction error is increased with respect to the optimal reference reconstructions $\\hat{A}$ and $\\hat{B}$. \n",
    "\n",
    "**Fairness Criterion:**\n",
    "The goal of Fair PCA (see Def. 4.4 in the above-mentioned paper) is to find a rank $d$ reconstruction $U$ that minimizes the larger of the two *average* group losses, i.e.\n",
    "$$\n",
    "\\min_{U \\in \\mathbb{R}^{m \\times n}, \\,\\text{rank}(U)\\leq d} \n",
    "\\max \n",
    "\\Bigl\\{\n",
    "\\frac{1}{\\lvert A \\rvert} \\text{loss}(A, U_A), \n",
    "\\frac{1}{\\lvert B \\rvert} \\text{loss}(B, U_B)\n",
    "\\Bigr\\}.\n",
    "$$\n",
    "\n",
    "The intuition behind this fairness criterion is that both groups, on average, \"pay the same price\" for the dimensionality reduction. The min-max optimization problem can be solved approximately by the so-called *Multiplicative Weight* algorithm. \n",
    "\n",
    "\n",
    "\n",
    "#### Part 1) Multiplicative Weight (MW)\n",
    "\n",
    "**Core idea:** The core idea of this algorithm is to find \"weights\" for the groups $A$ and $B$ such that the average loss on both groups is the same (or at least similar). The pseudo-code given below is copied from the supplementary material of the original paper (see above).\n",
    "\n",
    "<br />\n",
    "\n",
    "<div>\n",
    "<img src=\"Algo3_MW.PNG\" width=\"600\"/>\n",
    "</div>\n",
    "\n",
    "<br />\n",
    "\n",
    "Let us take a closer look at lines 1-7.\n",
    "- **Step 1:** We start with equal initial weights $p^0$.\n",
    "- **Step 2-6:**  The MW algorithm is an iterative approach. In each of the $T$ iterations, we try to improve/refine our choice of weights $p^t$.\n",
    "- **Step 3:**  The oracle function (see below) applies PCA to the \"weighted\" dataset. It returns a projection matrix $P_t$ and the corresponding average group losses $m_1^t$ (group $A$) and $m_2^t$ (group $B$). MW will try to make these two numbers small and as close as possible. \n",
    "- **Step 4:**  Here, the weights are rescaled based on the corresponding loss value. Let's assume $m_1^t$ is large compared to $m_2^t$. So, $A$ seems to be \"underrepresented\" in the weighted dataset - in order to make the group losses match, we have to give group $A$ a larger weight. \n",
    "- **Step 5:**  After the rescaling, we renormalize such that the weights add up to $1$ again. \n",
    "- **Step 7:**  If we are \"unlucky\", the algorithm will tune the weights too aggressively, i.e. both weights (and the average group loss values) will jump back and forth between small and large values. Thus, in order to make the algorithm more robust, we average over all projection matrices in the last step. $\\langle \\,\\cdot\\,, \\,\\cdot\\,\\rangle$ denotes the Frobenius inner product $\\langle X, Y \\rangle = \\sum_{i,j} X_{i,j} \\cdot Y_{i,j}$. The arguments of the maximum correspond to the average loss values for the reconstructions using the projection matrix $P^*$, i.e. $\\frac{1}{m_1}\\text{loss}(A, A\\cdot P^*)$ and $\\frac{1}{m_2}\\text{loss}(B, B\\cdot P^*)$. \n",
    "\n",
    "**Task:** Implement the MW algorithm. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mw(alpha, beta, A, B, eta, T, d):\n",
    "    \"\"\"\n",
    "    Multiplicative Weight algorithm.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    alpha, beta : float\n",
    "        Squared Frobenius norm of A_hat and B_hat respectively\n",
    "    A : numpy.ndarray with shape=(m1, n)\n",
    "        Data for group A\n",
    "    B : numpy.ndarray with shape=(m2, n)\n",
    "        Data for group B\n",
    "    eta : float > 0\n",
    "        Parameter for MW algorithm\n",
    "    T : int > 0\n",
    "        Number of iterations\n",
    "    d : int > 0\n",
    "        Rank of reconstruction matrix\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    P_star : numpy.ndarray with shape=(n, n)\n",
    "        Fair projection matrix\n",
    "    z_star : float\n",
    "        Maximum of corresponding average group loss values \n",
    "    \"\"\"\n",
    "    \n",
    "    # TODO\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Part 2) Oracle\n",
    "\n",
    "The next step will be to implement the oracle-function. Again, the pseudo-code given below is copied from the supplementary material of the original paper (see above).\n",
    "\n",
    "<br />\n",
    "\n",
    "<div>\n",
    "<img src=\"Algo2_Oracle.PNG\" width=\"600\"/>\n",
    "</div>\n",
    "\n",
    "<br />\n",
    "\n",
    "- **Step 1:** We are given weights $p = (p_1, p_2)$. In the first step, we weight the group covariance matrices $\\frac{1}{m_1} A^{\\text{T}} A$ and $\\frac{1}{m_2} B^{\\text{T}} B$ by $p_1$ and $p_2$ respectively. This matrix corresponds to the covariance of the block matrix \n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "\\sqrt{p_1 \\frac{m}{m_1}} A\\\\\n",
    "\\sqrt{p_2 \\frac{m}{m_2}} B\n",
    "\\end{pmatrix}\n",
    "\\quad \\text{with} \\quad m = m_1 + m_2\n",
    "$$ \n",
    "So, you can basically think of this step as applying PCA to our LFW data $\\begin{pmatrix}A\\\\B\\end{pmatrix}$, where the groups are rescaled according to their relative sizes and weights. You can use `numpy.svd` for the computation of the left-singular vectors. \n",
    "\n",
    "- **Step 2:** Here, we compute the projection matrix $P^*$ and the corresponding loss values. \n",
    "\n",
    "**Task:** Implement the oracle algorithm. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def oracle(p, alpha, beta, A, B, d):\n",
    "    \"\"\"\n",
    "    Oracle function: Apply PCA to sum of \"weighted\" group covariance matrices\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    p : tuple with len(p)=2\n",
    "        Relative weights p=(p_1, p_2) with p_1 + p_2 = 1\n",
    "    alpha, beta : float\n",
    "        Squared Frobenius norm of A_hat and B_hat respectively\n",
    "    A : numpy.ndarray with shape=(m1, n)\n",
    "        Data for group A\n",
    "    B : numpy.ndarray with shape=(m2, n)\n",
    "        Data for group B\n",
    "    d : int > 0\n",
    "        Rank of reconstruction matrix\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    P_star : numpy.ndarray with shape=(n, n)\n",
    "        Projection matrix for sum of weighted group covariance matrices\n",
    "    z1_star, z2_star : float\n",
    "        Corresponding average group loss values \n",
    "    \"\"\"\n",
    "    \n",
    "    # TODO\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Part 3) Fair PCA\n",
    "\n",
    "Let's put it all together..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fair_pca(A, B, eta, T, d): \n",
    "    \"\"\"\n",
    "    Fair PCA function: Apply Fair PCA algorithm\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    A : numpy.ndarray with shape=(m1, n)\n",
    "        Data for group A\n",
    "    B : numpy.ndarray with shape=(m2, n)\n",
    "        Data for group B\n",
    "    eta : float > 0\n",
    "        Parameter for MW algorithm\n",
    "    T : int > 0\n",
    "        Number of iterations in MW algorithm\n",
    "    d : int > 0\n",
    "        Rank of reconstruction matrix\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    P_star : numpy.ndarray with shape=(n, n)\n",
    "        Fair projection matrix\n",
    "    \"\"\"\n",
    "    \n",
    "    # Start fair PCA\n",
    "    print(f\"Fair PCA started (d = {d})\")\n",
    "    \n",
    "    # Compute optimal reconstructions A_hat and B_hat\n",
    "    P_A   = pca(A, d)\n",
    "    A_hat = A @ P_A\n",
    "    alpha = np.linalg.norm(A_hat, ord='fro')**2 / np.shape(A)[0]\n",
    "    \n",
    "    P_B   = pca(B, d)\n",
    "    B_hat = B @ P_B\n",
    "    beta  = np.linalg.norm(B_hat, ord='fro')**2 / np.shape(B)[0]\n",
    "    \n",
    "    # Call multiplicative weight algorithm and return projection matrix\n",
    "    P_fair, z_fair = mw(alpha, beta, A, B, eta/d, T, d)\n",
    "    print(f\"Fair PCA finished. Final loss = {z_fair:.3f}\")\n",
    "    return P_fair"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PCA vs. Fair PCA\n",
    "\n",
    "Now, let's see if our implementation of Fair PCA actually works and leads to a more balanced loss for both groups. For this, we are going to compare three different approaches:\n",
    "1. Apply PCA to A and B *separately* and obtain the optimal reconstructions $\\hat{A}$ and $\\hat{B}$ (in this case, the loss will be zero).\n",
    "2. Compute PCA of M and apply the projection matrix to both $A$ and $B$. This corresponds to the \"classical\" PCA approach and, as you have seen in the lecture, results in different losses for men and women.  \n",
    "3. Apply the Fair PCA algorithm. \n",
    "\n",
    "**Task:** Compute the average reconstruction error for all approaches and the average loss values for approaches 2 and 3. Show two subplots: One for the reconstruction errors and one for the losses over $d \\in \\{ 1, ..., 10\\}$. The result should look something like this.\n",
    "\n",
    "<br />\n",
    "\n",
    "<div>\n",
    "<img src=\"Error_and_Loss.PNG\" width=\"950\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parameters\n",
    "d_list = list(range(1,11))\n",
    "eta = 20\n",
    "T = 5\n",
    "\n",
    "# TODO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualization\n",
    "\n",
    "# TODO"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
